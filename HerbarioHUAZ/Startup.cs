﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HerbarioHUAZ.Startup))]
namespace HerbarioHUAZ
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
