﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HerbarioHUAZ;

namespace HerbarioHUAZ.Controllers
{
    public class CATALOGOesController : Controller
    {
        private MERHerbarioContainer db = new MERHerbarioContainer();

        // GET: CATALOGOes
        public ActionResult Index()
        {
            return View(db.CATALOGOSet.ToList());
        }

        // GET: CATALOGOes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CATALOGO cATALOGO = db.CATALOGOSet.Find(id);
            if (cATALOGO == null)
            {
                return HttpNotFound();
            }
            return View(cATALOGO);
        }

        // GET: CATALOGOes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CATALOGOes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Catalogo,Descripcion,Imagen")] CATALOGO cATALOGO)
        {
            if (ModelState.IsValid)
            {
                db.CATALOGOSet.Add(cATALOGO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cATALOGO);
        }

        // GET: CATALOGOes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CATALOGO cATALOGO = db.CATALOGOSet.Find(id);
            if (cATALOGO == null)
            {
                return HttpNotFound();
            }
            return View(cATALOGO);
        }

        // POST: CATALOGOes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Catalogo,Descripcion,Imagen")] CATALOGO cATALOGO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cATALOGO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cATALOGO);
        }

        // GET: CATALOGOes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CATALOGO cATALOGO = db.CATALOGOSet.Find(id);
            if (cATALOGO == null)
            {
                return HttpNotFound();
            }
            return View(cATALOGO);
        }

        // POST: CATALOGOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CATALOGO cATALOGO = db.CATALOGOSet.Find(id);
            db.CATALOGOSet.Remove(cATALOGO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
