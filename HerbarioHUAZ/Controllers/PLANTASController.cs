﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HerbarioHUAZ;

namespace HerbarioHUAZ.Controllers
{
    public class PLANTASController : Controller
    {
        private MERHerbarioContainer db = new MERHerbarioContainer();

        // GET: PLANTAS
        public ActionResult Index()
        {
            return View(db.PLANTASSet.ToList());
        }

        // GET: PLANTAS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PLANTAS pLANTAS = db.PLANTASSet.Find(id);
            if (pLANTAS == null)
            {
                return HttpNotFound();
            }
            return View(pLANTAS);
        }

        // GET: PLANTAS/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PLANTAS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Plantas,NC_Planta,ImagenPlanta,Fecha_Colecta")] PLANTAS pLANTAS)
        {
            if (ModelState.IsValid)
            {
                db.PLANTASSet.Add(pLANTAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pLANTAS);
        }

        // GET: PLANTAS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PLANTAS pLANTAS = db.PLANTASSet.Find(id);
            if (pLANTAS == null)
            {
                return HttpNotFound();
            }
            return View(pLANTAS);
        }

        // POST: PLANTAS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Plantas,NC_Planta,ImagenPlanta,Fecha_Colecta")] PLANTAS pLANTAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pLANTAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pLANTAS);
        }

        // GET: PLANTAS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PLANTAS pLANTAS = db.PLANTASSet.Find(id);
            if (pLANTAS == null)
            {
                return HttpNotFound();
            }
            return View(pLANTAS);
        }

        // POST: PLANTAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PLANTAS pLANTAS = db.PLANTASSet.Find(id);
            db.PLANTASSet.Remove(pLANTAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
