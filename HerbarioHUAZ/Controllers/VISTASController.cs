﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HerbarioHUAZ;

namespace HerbarioHUAZ.Controllers
{
    public class VISTASController : Controller
    {
        private MERHerbarioContainer db = new MERHerbarioContainer();

        // GET: VISTAS
        public ActionResult Index()
        {
            return View(db.VISTASSet.ToList());
        }

        // GET: VISTAS/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VISTAS vISTAS = db.VISTASSet.Find(id);
            if (vISTAS == null)
            {
                return HttpNotFound();
            }
            return View(vISTAS);
        }

        // GET: VISTAS/Create
        public ActionResult Create()
        {
            return View();
        }
        //DIRECCION DE PAGINAS
        public ActionResult Mision()
        {
            return View();
        }

        public ActionResult Vision()
        {
            return View();
        }

        public ActionResult ValoresCorporativos()
        {
            return View();
        }

        public ActionResult Ubicacion()
        {
            return View();
        }

        public ActionResult Portafolio()
        {
            return View();
        }
        public ActionResult Catalogo()
        {
            return View();
        }
        public ActionResult Noticias()
        {
            return View();
        }
        public ActionResult Articulo()
        {
            return View();
        }


        // POST: VISTAS/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Vistas,Nombre_Vista,DescripcionVista")] VISTAS vISTAS)
        {
            if (ModelState.IsValid)
            {
                db.VISTASSet.Add(vISTAS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vISTAS);
        }

        // GET: VISTAS/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VISTAS vISTAS = db.VISTASSet.Find(id);
            if (vISTAS == null)
            {
                return HttpNotFound();
            }
            return View(vISTAS);
        }

        // POST: VISTAS/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Vistas,Nombre_Vista,DescripcionVista")] VISTAS vISTAS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vISTAS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vISTAS);
        }

        // GET: VISTAS/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VISTAS vISTAS = db.VISTASSet.Find(id);
            if (vISTAS == null)
            {
                return HttpNotFound();
            }
            return View(vISTAS);
        }

        // POST: VISTAS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VISTAS vISTAS = db.VISTASSet.Find(id);
            db.VISTASSet.Remove(vISTAS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
