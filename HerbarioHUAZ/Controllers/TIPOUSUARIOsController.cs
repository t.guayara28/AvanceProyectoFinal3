﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HerbarioHUAZ;

namespace HerbarioHUAZ.Controllers
{
    public class TIPOUSUARIOsController : Controller
    {
        private MERHerbarioContainer db = new MERHerbarioContainer();

        // GET: TIPOUSUARIOs
        public ActionResult Index()
        {
            return View(db.TIPOUSUARIOSet.ToList());
        }

        // GET: TIPOUSUARIOs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPOUSUARIO tIPOUSUARIO = db.TIPOUSUARIOSet.Find(id);
            if (tIPOUSUARIO == null)
            {
                return HttpNotFound();
            }
            return View(tIPOUSUARIO);
        }

        // GET: TIPOUSUARIOs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TIPOUSUARIOs/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_TipoUsuario,Nombre_TU,ApellidoTU")] TIPOUSUARIO tIPOUSUARIO)
        {
            if (ModelState.IsValid)
            {
                db.TIPOUSUARIOSet.Add(tIPOUSUARIO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tIPOUSUARIO);
        }

        // GET: TIPOUSUARIOs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPOUSUARIO tIPOUSUARIO = db.TIPOUSUARIOSet.Find(id);
            if (tIPOUSUARIO == null)
            {
                return HttpNotFound();
            }
            return View(tIPOUSUARIO);
        }

        // POST: TIPOUSUARIOs/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_TipoUsuario,Nombre_TU,ApellidoTU")] TIPOUSUARIO tIPOUSUARIO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tIPOUSUARIO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tIPOUSUARIO);
        }

        // GET: TIPOUSUARIOs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TIPOUSUARIO tIPOUSUARIO = db.TIPOUSUARIOSet.Find(id);
            if (tIPOUSUARIO == null)
            {
                return HttpNotFound();
            }
            return View(tIPOUSUARIO);
        }

        // POST: TIPOUSUARIOs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TIPOUSUARIO tIPOUSUARIO = db.TIPOUSUARIOSet.Find(id);
            db.TIPOUSUARIOSet.Remove(tIPOUSUARIO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
