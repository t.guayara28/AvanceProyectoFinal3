﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HerbarioHUAZ;

namespace HerbarioHUAZ.Controllers
{
    public class ARTUCULOesController : Controller
    {
        private MERHerbarioContainer db = new MERHerbarioContainer();

        // GET: ARTUCULOes
        public ActionResult Index()
        {
            return View(db.ARTUCULOSet.ToList());
        }

        // GET: ARTUCULOes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ARTUCULO aRTUCULO = db.ARTUCULOSet.Find(id);
            if (aRTUCULO == null)
            {
                return HttpNotFound();
            }
            return View(aRTUCULO);
        }

        // GET: ARTUCULOes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ARTUCULOes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Articulo,Nom_Articulo,Numero_Paginas,Resumen_Articulo")] ARTUCULO aRTUCULO)
        {
            if (ModelState.IsValid)
            {
                db.ARTUCULOSet.Add(aRTUCULO);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(aRTUCULO);
        }

        // GET: ARTUCULOes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ARTUCULO aRTUCULO = db.ARTUCULOSet.Find(id);
            if (aRTUCULO == null)
            {
                return HttpNotFound();
            }
            return View(aRTUCULO);
        }

        // POST: ARTUCULOes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Articulo,Nom_Articulo,Numero_Paginas,Resumen_Articulo")] ARTUCULO aRTUCULO)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aRTUCULO).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(aRTUCULO);
        }

        // GET: ARTUCULOes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ARTUCULO aRTUCULO = db.ARTUCULOSet.Find(id);
            if (aRTUCULO == null)
            {
                return HttpNotFound();
            }
            return View(aRTUCULO);
        }

        // POST: ARTUCULOes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ARTUCULO aRTUCULO = db.ARTUCULOSet.Find(id);
            db.ARTUCULOSet.Remove(aRTUCULO);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
