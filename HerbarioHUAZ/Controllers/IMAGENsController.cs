﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HerbarioHUAZ;

namespace HerbarioHUAZ.Controllers
{
    public class IMAGENsController : Controller
    {
        private MERHerbarioContainer db = new MERHerbarioContainer();

        // GET: IMAGENs
        public ActionResult Index()
        {
            return View(db.IMAGENSet.ToList());
        }

        // GET: IMAGENs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IMAGEN iMAGEN = db.IMAGENSet.Find(id);
            if (iMAGEN == null)
            {
                return HttpNotFound();
            }
            return View(iMAGEN);
        }

        // GET: IMAGENs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IMAGENs/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id_Imagen,Nombre_Imagen,Desc_Imagen")] IMAGEN iMAGEN)
        {
            if (ModelState.IsValid)
            {
                db.IMAGENSet.Add(iMAGEN);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iMAGEN);
        }

        // GET: IMAGENs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IMAGEN iMAGEN = db.IMAGENSet.Find(id);
            if (iMAGEN == null)
            {
                return HttpNotFound();
            }
            return View(iMAGEN);
        }

        // POST: IMAGENs/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id_Imagen,Nombre_Imagen,Desc_Imagen")] IMAGEN iMAGEN)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iMAGEN).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iMAGEN);
        }

        // GET: IMAGENs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IMAGEN iMAGEN = db.IMAGENSet.Find(id);
            if (iMAGEN == null)
            {
                return HttpNotFound();
            }
            return View(iMAGEN);
        }

        // POST: IMAGENs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IMAGEN iMAGEN = db.IMAGENSet.Find(id);
            db.IMAGENSet.Remove(iMAGEN);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
