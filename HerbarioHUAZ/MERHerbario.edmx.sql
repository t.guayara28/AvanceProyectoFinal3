
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/30/2015 10:45:28
-- Generated from EDMX file: d:\users\tatiana\documents\visual studio 2013\Projects\HerbarioHUAZ\HerbarioHUAZ\MERHerbario.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [master];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'USUARIOSet'
CREATE TABLE [dbo].[USUARIOSet] (
    [Id_Usuario] int IDENTITY(1,1) NOT NULL,
    [Nom_Usuario] nvarchar(max)  NOT NULL,
    [Ape_Usuario] nvarchar(max)  NOT NULL,
    [Tel_Usuario] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'TIPOUSUARIOSet'
CREATE TABLE [dbo].[TIPOUSUARIOSet] (
    [Id_TipoUsuario] int IDENTITY(1,1) NOT NULL,
    [Nombre_TU] nvarchar(max)  NOT NULL,
    [ApellidoTU] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ARTUCULOSet'
CREATE TABLE [dbo].[ARTUCULOSet] (
    [Id_Articulo] int IDENTITY(1,1) NOT NULL,
    [Nom_Articulo] nvarchar(max)  NOT NULL,
    [Numero_Paginas] nvarchar(max)  NOT NULL,
    [Resumen_Articulo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CATALOGOSet'
CREATE TABLE [dbo].[CATALOGOSet] (
    [Id_Catalogo] int IDENTITY(1,1) NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [Imagen] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PLANTASSet'
CREATE TABLE [dbo].[PLANTASSet] (
    [Id_Plantas] int IDENTITY(1,1) NOT NULL,
    [NC_Planta] nvarchar(max)  NOT NULL,
    [ImagenPlanta] nvarchar(max)  NOT NULL,
    [Fecha_Colecta] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_Usuario] in table 'USUARIOSet'
ALTER TABLE [dbo].[USUARIOSet]
ADD CONSTRAINT [PK_USUARIOSet]
    PRIMARY KEY CLUSTERED ([Id_Usuario] ASC);
GO

-- Creating primary key on [Id_TipoUsuario] in table 'TIPOUSUARIOSet'
ALTER TABLE [dbo].[TIPOUSUARIOSet]
ADD CONSTRAINT [PK_TIPOUSUARIOSet]
    PRIMARY KEY CLUSTERED ([Id_TipoUsuario] ASC);
GO

-- Creating primary key on [Id_Articulo] in table 'ARTUCULOSet'
ALTER TABLE [dbo].[ARTUCULOSet]
ADD CONSTRAINT [PK_ARTUCULOSet]
    PRIMARY KEY CLUSTERED ([Id_Articulo] ASC);
GO

-- Creating primary key on [Id_Catalogo] in table 'CATALOGOSet'
ALTER TABLE [dbo].[CATALOGOSet]
ADD CONSTRAINT [PK_CATALOGOSet]
    PRIMARY KEY CLUSTERED ([Id_Catalogo] ASC);
GO

-- Creating primary key on [Id_Plantas] in table 'PLANTASSet'
ALTER TABLE [dbo].[PLANTASSet]
ADD CONSTRAINT [PK_PLANTASSet]
    PRIMARY KEY CLUSTERED ([Id_Plantas] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------